#!/usr/bin/env python3

import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
from tensorflow.python.client import device_lib

# print(tf.reduce_sum(tf.random.normal([1000, 1000])))
print(tf.config.list_physical_devices('GPU'))
print('looking for GPUs')
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    print("Name:", gpu.name, "  Type:", gpu.device_type)

# https://stackoverflow.com/a/38580201/7609868
print('looking for GPUs again')
local_device_protos = device_lib.list_local_devices()
for x in local_device_protos:
    if x.device_type == 'GPU':
        print(x.physical_device_desc)
