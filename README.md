# tfgputest

## Introduction

This repository provides some Python scripts to test whether tensorflow supports the GPU and
how faster the GPU is than the CPU.

## How to use

* You have to install Python on your system (e.g. `sudo apt install python3`).
* You need the appropriate venv-package on your system (e.g. `sudo apt install python3.10-venv`).
* Create a virtual environment `venv`.
* Install the required packages inside this `venv`.

You can call `source setup_env.sh` to create the virtual environment and install the required packages.

## Choose tensorflow version

The version of tensorflow is defined in file `requirements.txt`. To install the current version,
just do not provide a version information. If you want to install a specific version, define its
number behind the tensorflow package, e.g. `tensorflow[and-cuda]==2.15.1`.

## Call the Scripts

### tfgputest

```commandline
(venv) [~/src/tfgputest (main)]$ ./tfgputest.py
2024-05-14 11:34:46.357163: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered
2024-05-14 11:34:46.357187: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered
2024-05-14 11:34:46.357788: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered
[PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')]
looking for GPUs
Name: /physical_device:GPU:0   Type: GPU
looking for GPUs again
device: 0, name: NVIDIA GeForce RTX 3060 Laptop GPU, pci bus id: 0000:01:00.0, compute capability: 8.6
```

### cpu_vs_gpu

```commandline
(venv) [~/src/tfgputest (main)]$ ./cpu_vs_gpu.py
2024-05-14 11:36:16.549045: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered
2024-05-14 11:36:16.549068: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered
2024-05-14 11:36:16.549672: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered
Time (s) to convolve 32x7x7x3 filter over random 100x100x100x3 images (batch x height x width x channel). Sum of ten runs.
CPU (s):
0.2938777500003198
GPU (s):
0.057964608999100165
GPU speedup over CPU: 5x
```

## Alternative: use `env_vars.sh` for tensorflow 2.16.1

For a detailled discussion about the problems with version 2.16.1 check [Stack Overflow](https://stackoverflow.com/a/78468589/7609868).

* use the line without the version information in `requirements.txt`
```
## tensorflow[and-cuda]==2.15.1
tensorflow[and-cuda]
```
* install the new package
```
source setup_env.sh
```
* set the new values for `LD_LIBRARY_PATH`
```
source env_vars.sh
```
* Now the GPU will be found  
```
(venv) [~/src/tfgputest (main)]$ ./tfgputest.py
[PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')]
looking for GPUs
Name: /physical_device:GPU:0   Type: GPU
looking for GPUs again
device: 0, name: NVIDIA GeForce RTX 3060 Laptop GPU, pci bus id: 0000:01:00.0, compute capability: 8.6
```

## Error Handling

### `CUDA_ERROR_UNKNOWN: unknown error`

The error

```
(venv) [~/src/tfgputest (main)]$ ./tfgputest.py 
2024-06-02 18:01:53.091779: E external/local_xla/xla/stream_executor/cuda/cuda_driver.cc:282] failed call to cuInit: CUDA_ERROR_UNKNOWN: unknown error
[]
looking for GPUs
looking for GPUs again
```

can be solved with (cf. [Github](https://github.com/tensorflow/tensorflow/issues/53341))

```
(venv) [~/src/tfgputest (main)]$ sudo modprobe -r nvidia_uvm
(venv) [~/src/tfgputest (main)]$ sudo modprobe nvidia_uvm
(venv) [~/src/tfgputest (main)]$ ./tfgputest.py 
[PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')]
looking for GPUs
Name: /physical_device:GPU:0   Type: GPU
looking for GPUs again
device: 0, name: NVIDIA GeForce MX150, pci bus id: 0000:01:00.0, compute capability: 6.1
```

### Unable to register cuFFT, cuDNN, cuBLAS

This topic is discussed on [Github](https://github.com/tensorflow/tensorflow/issues/62075).

### successful NUMA node read from SysFS had negative value (-1)

cf. [Stack Overflow](https://stackoverflow.com/a/70225257/7609868)

```
root@t480huh:~# lspci -D | grep NVIDIA
0000:01:00.0 3D controller: NVIDIA Corporation GP108M [GeForce MX150] (rev a1)
root@t480huh:~# echo 0 | tee -a "/sys/bus/pci/devices/0000:01:00.0/numa_node"
0
```

Automatic activation after reboot using cron: add the following line to the crontab using `sudo crontab -e`:

```
@reboot (echo 0 | tee -a "/sys/bus/pci/devices/0000:01:00.0/numa_node")
```

with `0000:01:00.0` found by the `lspci` call mentioned above.
