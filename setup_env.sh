#!/bin/bash

# sudo apt install python3.10-venv
python3 -m venv venv
source venv/bin/activate
venv/bin/pip install --upgrade pip
venv/bin/pip install -r requirements.txt -U
